router.get('/', function(request, response, next) {
	Promise.try(function() {
		return AuctionController.getAll();
	}).then(function(data) {
		//response.send(data);
		res.render('list-auctions');
	}).catch(function(err) {
		response.status(500).send({error: err});
	});
});